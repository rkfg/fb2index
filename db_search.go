// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"log"

	"gitlab.com/opennota/fb2index/trigram"
)

var (
	trgmAuthorIndex   = trigram.NewIndex()
	trgmSequenceIndex = trigram.NewIndex()
	trgmBookIndex     = trigram.NewIndex()
)

func Search(query string) (authors []author, sequences []sequence, books []book, err error) {
	switch indexerType {
	case indexerInternal:
		trgm := trigram.Extract(query)
		authorIDs := trgmAuthorIndex.QueryTrigrams(trgm)
		sequenceIDs := trgmSequenceIndex.QueryTrigrams(trgm)
		bookIDs := trgmBookIndex.QueryTrigrams(trgm)

		if len(authorIDs) > 0 {
			authors = make([]author, 0, len(authorIDs))
		}
		if len(sequenceIDs) > 0 {
			sequences = make([]sequence, 0, len(sequenceIDs))
		}
		if len(bookIDs) > 0 {
			books = make([]book, 0, len(bookIDs))
		}

		for _, id := range authorIDs {
			a, err := AuthorByID(id)
			if err != nil {
				return nil, nil, nil, err
			}

			authors = append(authors, *a)
		}

		for _, id := range sequenceIDs {
			s, err := SequenceByID(id)
			if err != nil {
				return nil, nil, nil, err
			}

			sequences = append(sequences, *s)
		}

		for _, id := range bookIDs {
			b, err := BookByID(id)
			if err != nil {
				return nil, nil, nil, err
			}

			books = append(books, *b)
		}

	case indexerLike:
		var authorsList []author
		err := db.Select(&authorsList, `	SELECT 
					    id, first_name, middle_name, last_name, nickname 
					FROM 
					    authors 
					WHERE 
					    first_name LIKE ? OR
					    middle_name LIKE ? OR
					    last_name LIKE ? OR
					    nickname LIKE ?
					ORDER BY id`,
			"%"+query+"%",
			"%"+query+"%",
			"%"+query+"%",
			"%"+query+"%")
		if err != nil {
			log.Fatal(err)
		}
		authors = authorsList

		var sequenceList []sequence
		err = db.Select(&sequenceList, `SELECT 
					    id, name 
					FROM 
					    sequences 
					WHERE
					    name LIKE ?
					ORDER BY id`, "%"+query+"%")
		if err != nil {
			log.Fatal(err)
		}
		sequences = sequenceList

		var booksList []book
		err = db.Select(&booksList, `SELECT 
					    id, title 
					FROM 
					    books 
					WHERE
					    title LIKE ?
					ORDER BY id`, "%"+query+"%")
		if err != nil {
			log.Fatal(err)
		}

		var complexBooksList []book
		for _, b := range booksList {
			var bookAuthors []author
			err := db.Select(&bookAuthors, `SELECT 
						authors.id, authors.first_name, authors.middle_name, authors.last_name, authors.nickname 
					    FROM 
						authors, book_authors
					    WHERE 
						book_authors.book_id = ? AND book_authors.author_id = authors.id 
					    ORDER BY book_authors.author_id`, b.ID)
			if err != nil {
				log.Fatal(err)
			}
			b.Authors = bookAuthors

			var bookTranslators []author
			err = db.Select(&bookTranslators, `SELECT 
						    authors.id, authors.first_name, authors.middle_name, authors.last_name, authors.nickname 
						FROM 
						    authors, book_translators 
						WHERE 
						    book_translators.book_id = ? AND book_translators.author_id = authors.id 
						ORDER BY book_translators.author_id`, b.ID)
			if err != nil {
				log.Fatal(err)
			}
			b.Translators = bookTranslators

			var bookSequences []sequence
			err = db.Select(&bookSequences, `SELECT 
						    sequences.id, sequences.name 
						FROM 
						    sequences, book_sequences 
						WHERE 
						    book_sequences.book_id = ? AND book_sequences.sequence_id = sequences.id
						ORDER BY sequence_id`, b.ID)
			if err != nil {
				log.Fatal(err)
			}
			b.Sequences = bookSequences

			complexBooksList = append(complexBooksList, b)
		}

		books = complexBooksList

	case indexerFulltext:
	}

	return authors, sequences, books, nil
}
