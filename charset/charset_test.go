// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

package charset

import (
	"bytes"
	"strings"
	"testing"
)

var testCases = []struct {
	in   string
	cs   string
	want string
}{
	{"текст в кодировке utf-8", "utf-8", "текст в кодировке utf-8"},
	{"\xf2\xe5\xea\xf1\xf2\x20\xe2\x20\xea\xee\xe4\xe8\xf0\xee\xe2\xea\xe5\x20\x77\x69\x6e\x64\x6f\x77\x73\x2d\x31\x32\x35\x31", "windows-1251", "текст в кодировке windows-1251"},
	{"\xf2\xe5\xea\xf1\xf2\x20\xe2\x20\xea\xee\xe4\xe8\xf0\xee\xe2\xea\xe5\x20\x77\x69\x6e\x64\x6f\x77\x73\x2d\x31\x32\x35\x31", "Windows-1251", "текст в кодировке windows-1251"},
	{"\xd4\xc5\xcb\xd3\xd4\x20\xd7\x20\xcb\xcf\xc4\xc9\xd2\xcf\xd7\xcb\xc5\x20\x6b\x6f\x69\x38\x2d\x72", "koi8-r", "текст в кодировке koi8-r"},
	{"\xff\xfe\x42\x04\x35\x04\x3a\x04\x41\x04\x42\x04\x20\x00\x32\x04\x20\x00\x3a\x04\x3e\x04\x34\x04\x38\x04\x40\x04\x3e\x04\x32\x04\x3a\x04\x35\x04\x20\x00\x75\x00\x74\x00\x66\x00\x2d\x00\x31\x00\x36\x00", "utf-16", "текст в кодировке utf-16"},
	{"\x42\x04\x35\x04\x3a\x04\x41\x04\x42\x04\x20\x00\x32\x04\x20\x00\x3a\x04\x3e\x04\x34\x04\x38\x04\x40\x04\x3e\x04\x32\x04\x3a\x04\x35\x04\x20\x00\x75\x00\x74\x00\x66\x00\x2d\x00\x31\x00\x36\x00", "utf-16", "текст в кодировке utf-16"},
}

func Test(t *testing.T) {
	var buf bytes.Buffer
	for _, tc := range testCases {
		r, err := NewReader(tc.cs, strings.NewReader(tc.in))
		if err != nil {
			t.Error(err)
			continue
		}
		buf.Reset()
		if _, err := buf.ReadFrom(r); err != nil {
			t.Error(err)
			continue
		}
		if got := buf.String(); got != tc.want {
			t.Errorf("reading from charset %s: want %q, got %q", tc.cs, tc.want, got)
		}
	}
}
