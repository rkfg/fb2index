module gitlab.com/opennota/fb2index

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/jmoiron/sqlx v1.3.5
	github.com/lib/pq v1.10.4 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	golang.org/x/sys v0.0.0-20220730100132-1609e554cd39 // indirect
	golang.org/x/text v0.3.7
	golang.org/x/tools v0.1.12 // indirect
	gopkg.in/ini.v1 v1.66.6
	lukechampine.com/uint128 v1.2.0 // indirect
	modernc.org/ccgo/v3 v3.16.8 // indirect
	modernc.org/libc v1.16.19 // indirect
	modernc.org/opt v0.1.3 // indirect
	modernc.org/sqlite v1.18.0
	modernc.org/strutil v1.1.2 // indirect
)

go 1.16
