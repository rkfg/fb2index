// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"database/sql"
	_ "embed"
	"encoding/gob"
	"errors"
	"fmt"
	"log"
	"os"
	"time"

	mysql "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"gitlab.com/opennota/fb2index/trigram"
	ini "gopkg.in/ini.v1"
	sqlite3 "modernc.org/sqlite"
	sqlite3lib "modernc.org/sqlite/lib"
)

var (
	db *sqlx.DB

	ErrNoRows         = sql.ErrNoRows
	ErrAlreadyIndexed = errors.New("already indexed")

	engine       int
	engineSqlite = 0
	engineMysql  = 1

	indexerType     int
	indexerInternal = 10
	indexerLike     = 11
	indexerFulltext = 12
)

type Indexer struct {
	jobs                   <-chan book
	done                   chan bool
	tx                     *sqlx.Tx
	genreStmt              *sqlx.Stmt
	authorStmt             *sqlx.Stmt
	sequenceStmt           *sqlx.Stmt
	lastInsertIDStmt       *sqlx.Stmt
	insertGenreStmt        *sqlx.Stmt
	insertAuthorStmt       *sqlx.Stmt
	insertSequenceStmt     *sqlx.Stmt
	insertBookStmt         *sqlx.Stmt
	insertBookToGenreStmt  *sqlx.Stmt
	insertBookToAuthorStmt *sqlx.Stmt
	insertBookToTrStmt     *sqlx.Stmt
	insertBookToSeqStmt    *sqlx.Stmt
}

type TrigramIndexFile struct {
	AuthorIndex   trigram.Index
	SequenceIndex trigram.Index
	BookIndex     trigram.Index
}

func mustPreparex(tx *sqlx.Tx, sql string) *sqlx.Stmt {
	stmt, err := tx.Preparex(sql)
	if err != nil {
		panic(err)
	}
	return stmt
}

func NewIndexer(jobs <-chan book) *Indexer {
	tx := db.MustBegin()

	genreStmt := mustPreparex(tx, "SELECT id FROM genres WHERE name = ?")
	authorStmt := mustPreparex(tx, "SELECT id FROM authors WHERE last_name = ? AND first_name = ? AND middle_name = ? AND nickname = ?")
	sequenceStmt := mustPreparex(tx, "SELECT id FROM sequences WHERE name = ?")
	insertGenreStmt := mustPreparex(tx, "INSERT INTO genres (name, `desc`, meta) VALUES (?, '', '')")
	insertAuthorStmt := mustPreparex(tx, "INSERT INTO authors (first_name, middle_name, last_name, nickname) VALUES (?, ?, ?, ?)")
	insertSequenceStmt := mustPreparex(tx, "INSERT INTO sequences (name) VALUES (?)")
	insertBookStmt := mustPreparex(tx, "INSERT INTO books (title, lang, archive, filename, offset, compressed_size, uncompressed_size, crc32) VALUES (?, ?, ?, ?, ?, ?, ?, ?)")

	var lastInsertIDStmt *sqlx.Stmt
	var insertBookToGenreStmt *sqlx.Stmt
	var insertBookToAuthorStmt *sqlx.Stmt
	var insertBookToTrStmt *sqlx.Stmt
	var insertBookToSeqStmt *sqlx.Stmt

	//nolint:sqlclosecheck
	if engine == engineMysql {
		lastInsertIDStmt = mustPreparex(tx, "SELECT LAST_INSERT_ID()")
		insertBookToGenreStmt = mustPreparex(tx, "INSERT IGNORE INTO book_genres (book_id, genre_id) VALUES (?, ?)")
		insertBookToAuthorStmt = mustPreparex(tx, "INSERT IGNORE INTO book_authors (book_id, author_id) VALUES (?, ?)")
		insertBookToTrStmt = mustPreparex(tx, "INSERT IGNORE INTO book_translators (book_id, author_id) VALUES (?, ?)")
		insertBookToSeqStmt = mustPreparex(tx, "INSERT IGNORE INTO book_sequences (book_id, sequence_id, number) VALUES (?, ?, ?)")
	} else {
		lastInsertIDStmt = mustPreparex(tx, "SELECT last_insert_rowid()")
		insertBookToGenreStmt = mustPreparex(tx, "INSERT OR IGNORE INTO book_genres (book_id, genre_id) VALUES (?, ?)")
		insertBookToAuthorStmt = mustPreparex(tx, "INSERT OR IGNORE INTO book_authors (book_id, author_id) VALUES (?, ?)")
		insertBookToTrStmt = mustPreparex(tx, "INSERT OR IGNORE INTO book_translators (book_id, author_id) VALUES (?, ?)")
		insertBookToSeqStmt = mustPreparex(tx, "INSERT OR IGNORE INTO book_sequences (book_id, sequence_id, number) VALUES (?, ?, ?)")
	}

	d := &Indexer{
		jobs:                   jobs,
		done:                   make(chan bool),
		tx:                     tx,
		genreStmt:              genreStmt,
		authorStmt:             authorStmt,
		sequenceStmt:           sequenceStmt,
		lastInsertIDStmt:       lastInsertIDStmt,
		insertGenreStmt:        insertGenreStmt,
		insertAuthorStmt:       insertAuthorStmt,
		insertSequenceStmt:     insertSequenceStmt,
		insertBookStmt:         insertBookStmt,
		insertBookToGenreStmt:  insertBookToGenreStmt,
		insertBookToAuthorStmt: insertBookToAuthorStmt,
		insertBookToTrStmt:     insertBookToTrStmt,
		insertBookToSeqStmt:    insertBookToSeqStmt,
	}

	go d.worker()
	return d
}

func (d *Indexer) Done() <-chan bool { return d.done }

//nolint:unparam
func (d *Indexer) Close() error {
	_ = d.genreStmt.Close()
	_ = d.authorStmt.Close()
	_ = d.sequenceStmt.Close()
	_ = d.lastInsertIDStmt.Close()
	_ = d.insertGenreStmt.Close()
	_ = d.insertAuthorStmt.Close()
	_ = d.insertSequenceStmt.Close()
	_ = d.insertBookStmt.Close()
	_ = d.insertBookToGenreStmt.Close()
	_ = d.insertBookToAuthorStmt.Close()
	_ = d.insertBookToTrStmt.Close()
	_ = d.insertBookToSeqStmt.Close()

	return nil
}

func buildTrigrams() {
	// Make trigram indexes from the existing data.

	// Authors

	type author struct {
		ID         uint32
		FirstName  string `db:"first_name"`
		MiddleName string `db:"middle_name"`
		LastName   string `db:"last_name"`
		Nickname   string
	}

	var authors []author
	err := db.Select(&authors, "SELECT * FROM authors ORDER BY id")
	if err != nil {
		panic(err)
	}
	mAuthors := make(map[uint32][]trigram.T, len(authors))
	for _, a := range authors {
		tt := trigram.Extract(a.FirstName)
		tt = append(tt, trigram.Extract(a.MiddleName)...)
		tt = append(tt, trigram.Extract(a.LastName)...)
		tt = append(tt, trigram.Extract(a.Nickname)...)
		mAuthors[a.ID] = tt
		trgmAuthorIndex.AddTrigrams(a.ID, tt)
	}

	// Sequences
	type sequence struct {
		Name string
		ID   uint32
	}
	var sequences []sequence
	err = db.Select(&sequences, "SELECT * FROM sequences ORDER BY id")
	if err != nil {
		panic(err)
	}
	mSequences := make(map[uint32][]trigram.T, len(sequences))
	for _, s := range sequences {
		tt := trigram.Extract(s.Name)
		mSequences[s.ID] = tt
		trgmSequenceIndex.AddTrigrams(s.ID, tt)
	}

	// Books
	type book struct {
		ID    uint32
		Title string
	}
	var books []book
	err = db.Select(&books, "SELECT id, title FROM books ORDER BY id")
	if err != nil {
		panic(err)
	}

	authorsStmt, err := db.Preparex("SELECT author_id FROM book_authors WHERE book_id = ? ORDER BY author_id")
	if err != nil {
		panic(err)
	}
	defer authorsStmt.Close()

	translatorsStmt, err := db.Preparex("SELECT author_id FROM book_translators WHERE book_id = ? ORDER BY author_id")
	if err != nil {
		panic(err)
	}
	defer translatorsStmt.Close()

	sequencesStmt, err := db.Preparex("SELECT sequence_id FROM book_sequences WHERE book_id = ? ORDER BY sequence_id")
	if err != nil {
		panic(err)
	}
	defer sequencesStmt.Close()

	for _, b := range books {
		trgmBookIndex.Add(b.ID, b.Title)

		var authorIDs []uint32
		var translatorIDs []uint32
		var sequenceIDs []uint32

		err := authorsStmt.Select(&authorIDs, b.ID)
		if err != nil {
			panic(err)
		}
		for _, id := range authorIDs {
			trgmBookIndex.AddTrigrams(b.ID, mAuthors[id])
		}

		err = translatorsStmt.Select(&translatorIDs, b.ID)
		if err != nil {
			panic(err)
		}
		for _, id := range translatorIDs {
			trgmBookIndex.AddTrigrams(b.ID, mAuthors[id])
		}

		err = sequencesStmt.Select(&sequenceIDs, b.ID)
		if err != nil {
			panic(err)
		}
		for _, id := range sequenceIDs {
			trgmBookIndex.AddTrigrams(b.ID, mSequences[id])
		}
	}
}

//go:embed sql/sqlite_schema.sql
var sqliteSchema string

func initDB() {
	if *dataSource == "mysql" {
		engine = engineMysql
		mysqlIni, err := ini.Load("mysql.ini")
		if err != nil {
			panic(err)
		}
		var (
			user      = mysqlIni.Section("MYSQL").Key("user").String()
			password  = mysqlIni.Section("MYSQL").Key("password").String()
			host      = mysqlIni.Section("MYSQL").Key("host").String()
			port      = mysqlIni.Section("MYSQL").Key("port").String()
			database  = mysqlIni.Section("MYSQL").Key("database").String()
			tls       = mysqlIni.Section("MYSQL").Key("tls").MustInt(0)
			tlsSwitch string
		)

		if port != "" {
			host = fmt.Sprintf("%s:%s", host, port)
		}
		if database == "" {
			database = "fb2index"
		}
		switch tls {
		case 0:
			tlsSwitch = ""
		case 1:
			tlsSwitch = "tls=true"
		case 2:
			tlsSwitch = "tls=skip-verify"
		}

		ldb, err := sqlx.Open("mysql", fmt.Sprintf("%s:%s@(%s)/%s?%s", user, password, host, database, tlsSwitch))
		if err != nil {
			panic(err)
		}
		// See "Important settings" section.
		ldb.SetConnMaxLifetime(time.Minute * 3)
		ldb.SetMaxOpenConns(10)
		ldb.SetMaxIdleConns(10)
		db = ldb
	} else {
		engine = engineSqlite
		ldb := sqlx.MustConnect("sqlite", *dataSource)
		db = ldb
		db.MustExec(sqliteSchema)
	}

	if indexerType == indexerInternal {
		if *indexFile != "" {
			if _, err := os.Stat(*indexFile); os.IsNotExist(err) {
				buildTrigrams()
				if err := saveTrigrams(); err != nil {
					log.Printf("Error saving index: %v", err)
				}
			} else {
				if err := loadTrigrams(); err != nil {
					log.Printf("Error loading index: %v; rebuilding from scratch...", err)
					buildTrigrams()
				}
			}
		} else {
			buildTrigrams()
		}
	}
}

func saveTrigrams() error {
	f, err := os.Create(*indexFile)
	if err != nil {
		return fmt.Errorf("can't create file %s to save index to: %w", *indexFile, err)
	}
	defer f.Close()
	enc := gob.NewEncoder(f)
	idx := TrigramIndexFile{
		AuthorIndex:   trgmAuthorIndex,
		SequenceIndex: trgmSequenceIndex,
		BookIndex:     trgmBookIndex,
	}
	err = enc.Encode(idx)
	if err != nil {
		return fmt.Errorf("can't encode index to file %s: %w", *indexFile, err)
	}
	return nil
}

func loadTrigrams() error {
	f, err := os.Open(*indexFile)
	if err != nil {
		return fmt.Errorf("can't open file %s to load index from: %w", *indexFile, err)
	}
	defer f.Close()
	dec := gob.NewDecoder(f)
	var idx TrigramIndexFile
	err = dec.Decode(&idx)
	if err != nil {
		return fmt.Errorf("can't decode index from file %s: %w", *indexFile, err)
	}
	trgmAuthorIndex = idx.AuthorIndex
	trgmSequenceIndex = idx.SequenceIndex
	trgmBookIndex = idx.BookIndex
	return nil
}

func (d *Indexer) worker() {
	defer close(d.done)

	for book := range d.jobs {
		err := d.indexBook(book)
		if err != nil {
			if err == ErrAlreadyIndexed {
				log.Printf("%s/%s: already indexed", book.Archive, book.Filename)
			} else {
				log.Printf("%s/%s: failed to add book: %v", book.Archive, book.Filename, err)
			}
		}
	}

	err := d.tx.Commit()
	if err != nil {
		log.Printf("Commit failed: %v", err)
		if err2 := d.tx.Rollback(); err2 != nil {
			log.Printf("Rollback failed: %v", err2)
		}
	}
}

func (d *Indexer) lastInsertID() (id uint32, err error) {
	err = d.lastInsertIDStmt.Get(&id)
	return
}

func (d *Indexer) getOrInsertGenre(g string) (id uint32, inserted bool, err error) {
	err = d.genreStmt.Get(&id, g)
	if err != sql.ErrNoRows {
		return
	}

	_, err = d.insertGenreStmt.Exec(g)
	if err != nil {
		return
	}

	id, err = d.lastInsertID()
	return id, err == nil, err
}

func (d *Indexer) getOrInsertAuthor(a author) (id uint32, inserted bool, err error) {
	err = d.authorStmt.Get(&id, a.LastName, a.FirstName, a.MiddleName, a.Nickname)
	if err != sql.ErrNoRows {
		return
	}

	_, err = d.insertAuthorStmt.Exec(a.FirstName, a.MiddleName, a.LastName, a.Nickname)
	if err != nil {
		return
	}

	id, err = d.lastInsertID()
	return id, err == nil, err
}

func (d *Indexer) getOrInsertSequence(name string) (id uint32, inserted bool, err error) {
	err = d.sequenceStmt.Get(&id, name)
	if err != sql.ErrNoRows {
		return
	}

	_, err = d.insertSequenceStmt.Exec(name)
	if err != nil {
		return
	}

	id, err = d.lastInsertID()
	return id, err == nil, err
}

func (d *Indexer) indexBook(b book) error {
	_, err := d.insertBookStmt.Exec(b.Title, b.Lang, b.Archive, b.Filename, b.Offset, b.CompressedSize, b.UncompressedSize, b.CRC32)
	if err != nil {
		if engine == engineMysql {
			if sqlErr, ok := err.(*mysql.MySQLError); ok && sqlErr.Number == 1062 {
				return ErrAlreadyIndexed
			}
		} else {
			if sqlErr, ok := err.(*sqlite3.Error); ok && sqlErr.Code() == sqlite3lib.SQLITE_CONSTRAINT {
				return ErrAlreadyIndexed
			}
		}
		return err
	}

	bookID, err := d.lastInsertID()
	if err != nil {
		return err
	}

	var trigrams []trigram.T

	for _, g := range b.Genres {
		genreID, _, err := d.getOrInsertGenre(g.Name)
		if err != nil {
			return err
		}

		_, err = d.insertBookToGenreStmt.Exec(bookID, genreID)
		if err != nil {
			return err
		}
	}

	for _, a := range b.Authors {
		authorID, inserted, err := d.getOrInsertAuthor(a)
		if err != nil {
			return err
		}

		_, err = d.insertBookToAuthorStmt.Exec(bookID, authorID)
		if err != nil {
			return err
		}

		for _, s := range []string{a.FirstName, a.MiddleName, a.LastName, a.Nickname} {
			trgm := trigram.Extract(s)
			if inserted {
				trgmAuthorIndex.AddTrigrams(authorID, trgm)
			}
			trigrams = append(trigrams, trgm...)
		}
	}

	for _, a := range b.Translators {
		authorID, inserted, err := d.getOrInsertAuthor(a)
		if err != nil {
			return err
		}

		_, err = d.insertBookToTrStmt.Exec(bookID, authorID)
		if err != nil {
			return err
		}

		for _, s := range []string{a.FirstName, a.MiddleName, a.LastName, a.Nickname} {
			trgm := trigram.Extract(s)
			if inserted {
				trgmAuthorIndex.AddTrigrams(authorID, trgm)
			}
			trigrams = append(trigrams, trgm...)
		}
	}

	for _, s := range b.Sequences {
		seqID, inserted, err := d.getOrInsertSequence(s.Name)
		if err != nil {
			return err
		}

		_, err = d.insertBookToSeqStmt.Exec(bookID, seqID, s.Number)
		if err != nil {
			return err
		}

		trgm := trigram.Extract(s.Name)
		trigrams = append(trigrams, trgm...)
		if inserted {
			trgmSequenceIndex.AddTrigrams(seqID, trgm)
		}
	}

	trgmBookIndex.AddTrigrams(bookID, trigrams)
	trgmBookIndex.Add(bookID, b.Title)

	return nil
}
