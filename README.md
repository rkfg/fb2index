fb2index [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html) [![Pipeline status](https://gitlab.com/opennota/fb2index/badges/master/pipeline.svg)](https://gitlab.com/opennota/fb2index/commits/master)
========

[На русском](README.ru.md).

# Donate

**Bitcoin (BTC):** `1PEaahXKwJvNJGJa2PXtPFLNYYigmdLXct`

**Ethereum (ETH):** `0x83e9607E693467Cb344244Df10f66c036eC3Dc53`

![Screencast](/screencast.gif)

